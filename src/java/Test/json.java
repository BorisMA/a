/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test;

import Dto.Cliente;
import Dto.Cuenta;
import com.google.gson.Gson;
import java.time.LocalDate;

/**
 *
 * @author Usuario
 */
public class json {
    
    public static void main(String[] args) {
        Gson json = new Gson();
        Cliente c = new Cliente(12313, "boris", LocalDate.now(), "adsdadsa", 321312312, "asdas@adas");
        Cuenta d = new Cuenta(1, c, 12, LocalDate.now());
        
        String data = " {\"clientes\": [" + json.toJson(c) + "," + json.toJson(d) + "]}";
                
        
        System.out.println(data);
                
    }
}
