/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test;
import Dto.Cliente;
import com.google.gson.Gson;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.*;
import java.nio.file.Paths;
import java.time.LocalDate;


/**
 *
 * @author Usuario
 */
public class Files1 {
    
    /**
     * This class shows how to write file in java
     * @param args
     * @throws IOException 
     */
    public static void main(String[] args) {
        
        Gson json = new Gson();
        Cliente c = new Cliente(12313, "xd", LocalDate.now(), "dddd", 321312312, "asdas@adas");
        Cliente d = new Cliente(12313, "boris", LocalDate.now(), "cccc", 321312312, "asdas@adas");
                
        String data = " {\"clientes\": [" + json.toJson(c) + "," + json.toJson(d) + "]}";
        
       
        
        writeUsingFileWriter(data);
        
         System.out.println(data);
        System.out.println("DONE");
    }

    /**
     * Use FileWriter when number of write operations are less
     * @param data
     */
    private static void writeUsingFileWriter(String data) {
        
        File file = new File("web/json/clientes.json");
        //File file = new File("src/java/Util/clientes.json");
        FileWriter fr = null;
        try {
            fr = new FileWriter(file);
            fr.write(data);
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            //close resources
            try {
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
