/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test;

import Dto.Cliente;
import Dto.Cuenta;
import Dto.CuentaAhorro;
import Dto.CuentaCorriente;
import Dto.Operacion;
import java.time.LocalDate;
import java.util.TreeSet;

/**
 *
 * @author estudiante
 */
public class TestTreeset {
    public static void main(String[] args) {
        TreeSet<Cliente> clientes = new TreeSet();
        TreeSet<Operacion> op = new TreeSet();
        TreeSet<Cuenta> cuen = new TreeSet();
        
        Cliente x = new Cliente(123, "wqe", LocalDate.now(), "asd", 1231, "qweq");
        Cuenta c = new Cuenta(1, x, 100000, LocalDate.now());

        clientes.add(x);
        cuen.add(c);
    
        int saldo = 10000;
        Cuenta xa=new Cuenta();
        xa.setNroCuenta(1);
        if(cuen.contains(xa)){
            xa = cuen.floor(xa);
            xa.setSaldo(xa.getSaldo()+ saldo);
        }
    
        System.out.println(c);
    }
    
    
    
    }
