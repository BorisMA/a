/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Dto.*;
import com.google.gson.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.TreeSet;

/**
 *
 * @author docente
 */
public class Banco {
    TreeSet<Cliente> clientes=new TreeSet();
    TreeSet<Cuenta> cuentas=new TreeSet();
    TreeSet<Operacion> operaciones = new TreeSet();
    
    public Banco() {
    }
    
    public boolean insertarCliente(long cedula, String nombre, String dir, String fecha, String email, long telefono, String path) throws IOException
    {
    
        Cliente nuevo=new Cliente();
        nuevo.setCedula(cedula);
        nuevo.setNombre(nombre);
        nuevo.setFechaNacimiento(crearFecha(fecha));
        nuevo.setDirCorrespondencia(dir);
        nuevo.setTelefono(telefono);
        nuevo.setEmail(email);
        //iría al dao , pero sólo por hoy en dinamic_memory
        if (this.clientes.add(nuevo)) {
            añadirSelect(path);
            return true;
        }
        return false;
    }
    
    
    public boolean insertarCuenta(long nroCuenta, long cedula, boolean tipo, String path) throws IOException{
        for (Cliente cliente : clientes) {
            System.out.println(cliente.toString());
        }
    Cliente x=new Cliente();
    x.setCedula(cedula);
    x = buscarCliente(x);
    
    if(x==null){
        return false;
    }
    Cuenta nueva=tipo?new CuentaAhorro():new CuentaCorriente();
    nueva.setCliente(x);
    nueva.setNroCuenta(nroCuenta);
    nueva.setFechaCreacion(LocalDate.now());
        if (this.cuentas.add(nueva)) {
            añadirCuentasSelect(path);
            return true;
        }
    return false;
    }
    
    public boolean insertarTransferencia(int identificador, long nroCuenta1, long nroCuenta2, double saldo){
        Operacion ret = ret(comprobarId(identificador+1), saldo, nroCuenta1);
        Operacion cons = null;
        Operacion tran = null;
        if (ret!=null) {
            cons = cons(comprobarId(identificador+2), saldo, nroCuenta2);
            tran = new Transferencia(cons.getCuentaOrigen(), LocalDate.now(), saldo, identificador, ret.getCuentaOrigen());
            operaciones.add(tran);
            operaciones.add(ret);
            operaciones.add(cons);
            return true;
        }
        return false;
    }
    
    private int comprobarId(int identificador){
        Operacion aux = new Operacion();
        int id = identificador;
        aux.setIdentificador(id);
        while(operaciones.contains(aux)){
            id++;
            aux.setIdentificador(id);
        }
        return id;
    }
    
    public boolean insertarOperacion(int identificador, long nroCuenta, double saldo, int tipo){
    Operacion nuevo;
    
    switch(tipo){
        case 0 :
            nuevo = cons(identificador, saldo, nroCuenta);
            break;
        case 1 :
            nuevo = ret(identificador, saldo, nroCuenta);
            break;
        default:
            nuevo = null;
    }
    
        if (nuevo!=null) {
            this.operaciones.add(nuevo);
            return true;
        }
        
    return false;
    }
    
    private Operacion ret(int identificador, double saldo, long nroCuenta){
        double aux = saldo;
        Cuenta x=new Cuenta();
        x.setNroCuenta(nroCuenta);
        if(this.cuentas.contains(x)){
            x = this.cuentas.floor(x);
            if (x instanceof CuentaCorriente) {
                if(x.getSaldo() > aux){
                        x.setSaldo(x.getSaldo()-aux);
                }else if (x.getSaldo() + ((CuentaCorriente) x).getSobregiro() >= aux) {
                        aux -= x.getSaldo();
                        x.setSaldo(0);
                        ((CuentaCorriente) x).setSobregiro(((CuentaCorriente) x).getSobregiro()-aux);
                }else{
                    return null;
                }
            }else{
                if (x.getSaldo() > aux) {
                    x.setSaldo(x.getSaldo()-aux);
                }else{ 
                    return null;
                }
            }
        }
        return new Retiro(LocalDate.now(), saldo, identificador, x);
    }
    
    private Operacion cons(int identificador, double saldo, long nroCuenta){
        Cuenta x=new Cuenta();
        x.setNroCuenta(nroCuenta);
        if(this.cuentas.contains(x)){
            x = this.cuentas.floor(x);
            x.setSaldo(x.getSaldo()+saldo);
        }
        return new Consignacion(LocalDate.now(), saldo, identificador, x);
    }
    
    private Cliente buscarCliente(Cliente x){
        if(this.clientes.contains(x)){
            return this.clientes.floor(x);
        }
        return null;
    } 
    
    private void añadirSelect(String path) throws IOException{
        Gson gson = new Gson();
        String aux = "[";
        File file = new File(path);
        FileWriter fr = null;
        boolean first = true;
        
                
        for (Cliente c : clientes) {
            if (c != null) {
                if(first){
                    aux += gson.toJson(c);
                    first = false;
                }else{
                    aux += "," + gson.toJson(c);
                }
            }
        }
        aux += "]";
        
        try {
            fr = new FileWriter(file);
            fr.write(aux);
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            //close resources
            try {
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    private void añadirCuentasSelect(String path) throws IOException{
        Gson gson = new Gson();
        String aux = "[";
        File file = new File(path);
        FileWriter fr = null;
        boolean first = true;
        
                
        for (Cuenta c : cuentas) {
            if (c != null) {
                if(first){
                    aux += gson.toJson(c);
                    first = false;
                }else{
                    aux += "," + gson.toJson(c);
                }
            }
        }
        aux += "]";
        
        try {
            fr = new FileWriter(file);
            fr.write(aux);
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            //close resources
            try {
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    private LocalDate crearFecha(String fecha)
    {
        String fechas[]=fecha.split("-");
        int agno=Integer.parseInt(fechas[0]);
        int mes=Integer.parseInt(fechas[1]);
        int dia=Integer.parseInt(fechas[2]);
        return LocalDate.of(agno,mes,dia);
    }
    
    public TreeSet<Cliente> getClientes() {
        return clientes;
    }
    
    public TreeSet<Cuenta> getCuentas() {
        return cuentas;
    }
    
    public TreeSet<Operacion> getOperaciones(){
        return operaciones;
    }
}
