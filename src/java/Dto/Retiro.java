/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import java.time.LocalDate;

/**
 *
 * @author Usuario
 */
public class Retiro extends Operacion{

    public Retiro() {
        super();
    }

    public Retiro(LocalDate fecha, double saldo, int identificador, Cuenta cuentaOrigen) {
        super(fecha, saldo, identificador, cuentaOrigen);
    }
    
}
