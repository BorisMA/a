/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import java.time.LocalDate;

/**
 *
 * @author estudiante
 */
public class CuentaAhorro extends Cuenta{

    public CuentaAhorro() {
        super();
    }

    public CuentaAhorro(long nroCuenta, Cliente cliente, double saldo, LocalDate fechaCreacion) {
        super(nroCuenta, cliente, saldo, fechaCreacion);
    }
    
    
}
