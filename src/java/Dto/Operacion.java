/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import java.time.LocalDate;

/**
 *
 * @author Usuario
 */
public class Operacion implements Comparable{
    
    private LocalDate fecha;
    private double saldo;
    private int identificador;
    private Cuenta cuentaOrigen;

    public Operacion() {
    }

    public Operacion(LocalDate fecha, double saldo, int identificador, Cuenta cuentaOrigen) {
        this.fecha = fecha;
        this.saldo = saldo;
        this.identificador = identificador;
        this.cuentaOrigen = cuentaOrigen;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    public Cuenta getCuentaOrigen() {
        return cuentaOrigen;
    }

    public void setCuentaOrigen(Cuenta cuentaOrigen) {
        this.cuentaOrigen = cuentaOrigen;
    }
    
    @Override
    public int compareTo(Object o) {
      Operacion op=(Operacion)o;
      return (int)(this.identificador - op.identificador);
    }

    @Override
    public String toString() {
        return "Operacion = Identificador: " + identificador + ", Saldo: " + saldo + ", en la Cuenta: " + cuentaOrigen.getNroCuenta() + ", Fecha de realizacion: " + fecha;
    }
}
