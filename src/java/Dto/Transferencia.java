/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import java.time.LocalDate;

/**
 *
 * @author Usuario
 */
public class Transferencia extends Operacion{

    private Cuenta cuentaDestino;

    public Transferencia() {
        super();
    }

    public Transferencia(Cuenta cuentaDestino, LocalDate fecha, double saldo, int identificador, Cuenta cuentaOrigen) {
        super(fecha, saldo, identificador, cuentaOrigen);
        this.cuentaDestino = cuentaDestino;
    }

    public Cuenta getCuentaDestino() {
        return cuentaDestino;
    }

    public void setCuentaDestino(Cuenta cuentaDestino) {
        this.cuentaDestino = cuentaDestino;
    }

    @Override
    public String toString() {
        return super.toString() + " transferencia a la cuenta:" + cuentaDestino.getNroCuenta();
    }
    
    
    
    
}
