/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import java.time.LocalDate;

/**
 *
 * @author estudiante
 */
public class CuentaCorriente extends Cuenta{

   private double sobregiro=1000000; 
    
    public CuentaCorriente() {
        super();
    }

    public CuentaCorriente(long nroCuenta, Cliente cliente, double saldo, LocalDate fechaCreacion) {
        super(nroCuenta, cliente, saldo, fechaCreacion);
    }
    
    public double getSobregiro() {
        return sobregiro;
    }

    public void setSobregiro(double sobregiro) {
        this.sobregiro = sobregiro;
    }

    @Override
    public String toString() {
        return super.toString() + "Sobre giro restante: " + sobregiro; //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
